using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menager : MonoBehaviour
{
    private CameraMove cameraMove;
    private Shaker shaker;


    private void Awake()
    {
        shaker = GetComponentInChildren<Shaker>();
        cameraMove = GetComponent<CameraMove>();
    }
    private void Update()
    {
        shaker.ClickOnShaker();
        cameraMove.ChangeRoom();
    }
}
