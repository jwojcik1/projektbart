using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    private Vector3 worldMousePosition;
    private Vector3 baseBootlePosition;
    private Vector3 bootlePosition;
    private Vector3 mousePos;

    public bool isBeigHeld;

    [SerializeField] public Color color;
    [SerializeField] private Transform position;

    
    private void Update()
    {
        mousePos = Input.mousePosition;
        mousePos.z =Camera.main.nearClipPlane;
        worldMousePosition = Camera.main.ScreenToWorldPoint(mousePos);

        if (isBeigHeld)
        {
            bootlePosition = new Vector3(worldMousePosition.x, worldMousePosition.y, mousePos.z);
            transform.position = bootlePosition;
        }
        else
        {
            bootlePosition = transform.position;
            baseBootlePosition = position.position;
            transform.position = Vector3.MoveTowards(bootlePosition, baseBootlePosition, 1f);
        }
    }
    private void OnMouseDown()
    {
        if (Input.GetMouseButton(0))
        {
            mousePos = Input.mousePosition;
            mousePos.z = Camera.main.nearClipPlane;
            worldMousePosition = Camera.main.ScreenToWorldPoint(mousePos);

            isBeigHeld = true;
        }
    }
    private void OnMouseUp()
    {
        isBeigHeld = false;
    }
}
