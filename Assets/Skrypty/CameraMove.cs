using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    private float transitionCD = 0.45f;
    private bool canMove = true;
    private bool inRightRoom = false;
    private bool inLeftRoom = false;

    Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }
    public void ChangeRoom()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (!canMove || inRightRoom)
            { return; }
            animator.SetTrigger("Right");
            canMove = false;
            StartCoroutine(WaitToTransition());
        }
        else if (Input.GetKeyDown(KeyCode.Q))
        {
            if (!canMove || inLeftRoom)
            { return; }
            animator.SetTrigger("Left");
            canMove = false;
            StartCoroutine(WaitToTransition());
        }
    }
    private IEnumerator WaitToTransition()
    {
        yield return new WaitForSeconds(transitionCD);
        canMove = true;
    }
    public void InLeftRoom()
    {
        inLeftRoom = true;
        inRightRoom = false;
    }
    public void InRightRoom()
    {
        inLeftRoom = false;
        inRightRoom = true;
    }
    public void InMainRoom()
    {
        inLeftRoom = false;
        inRightRoom = false;
    }
}
