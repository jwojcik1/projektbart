using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shaker : MonoBehaviour
{

    [SerializeField] private GameObject[] bootles;
    [SerializeField] private Transform targetToLook;
    [SerializeField] private float speed;
    [SerializeField] private float rotationModifier;

    private DragObject dragObject;
    private Vector3 vectorTarget;
    private Vector3 mousePos;

    private const string _alkohol = "Alcohol";

    private void Awake()
    {
        dragObject = GetComponentInChildren<DragObject>();
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == _alkohol)
        {
                Debug.Log("nalewa: " + dragObject.gameObject.name + "color: "+dragObject.color);
                vectorTarget = targetToLook.position - collision.gameObject.transform.position;
                float angle = Mathf.Atan2(vectorTarget.y, vectorTarget.x) * Mathf.Rad2Deg - rotationModifier;
                Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
                collision.gameObject.transform.rotation = Quaternion.Slerp(collision.gameObject.transform.rotation, q, Time.deltaTime * speed);
        }
    }
    public void ClickOnShaker()
    {

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == _alkohol)
        {
            Quaternion q = Quaternion.EulerAngles(0, 0, 0);
            collision.gameObject.transform.rotation = q;
        }
    }
}
